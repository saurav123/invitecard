<div id="wrapper">
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo BASE_URL ?>/user/Dashboard">Welcome <?php echo $name; ?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo BASE_URL ?>/user/profile"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="<?php echo BASE_URL ?>/user/Setting"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo BASE_URL ?>/user/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="<?php echo BASE_URL ?>/user/Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo BASE_URL ?>/user/deactivated_event_view"><i class="fa fa-dashboard fa-fw"></i>Suspended Events</a>
                                </li>
                            </ul>
                        </li>
                        
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                      
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>



         <div id="page-wrapper" style="min-height: 339px;">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header">Events</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
                          
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Events Details
                            
                            <div class="pull-right"> 
                                <a href="<?php echo BASE_URL ?>/user/Dashboard" class="btn btn-default btn-xs">Go to Dashboard</a>
                            </div>
                            
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                <div class="col-md-4 col-md-offset-4"><img class="img-responsive img-rounded" src="<?php echo BASE_URL ?>/uploads/<?php echo $edits['Event']['img'] ?>"/></div>

                                </div>
                                <div style="margin-top:20px;" class="col-lg-12">
                                    <h1 style="text-transform: uppercase;" class="text-center"><?php echo $edits['Event']['title'] ?></h1>
                                </div>

                                <?php if (!empty($Ceremony)) { ?>

                                        <div style="margin-top:20px;" class="col-lg-12">
                                    <h1 style="text-transform: uppercase; font-weight: bold;" class="text-center">Ceremonies</h1>
                                </div><?php }else { ?>

                                            <h1 style="text-transform: uppercase; font-size: large; font-weight: lighter;" class="text-center">No ceremonies :(</h1>

                                            <?php }?>
                                        

                                <?php  $index = 1;  foreach ($Ceremony as $records) { ?>

                                  <div style="margin-top:20px;" class="col-lg-12">
                                    <h3 style="text-transform: uppercase; font-style: italic;font-size: large;" class="text-center"><?php echo $index++; ?>-<?php echo $records['Ceremony']['title']." is held on ".$records['Ceremony']['time'] . $records['Ceremony']['date']." at ". $records['Ceremony']['address']?></h3>
                                </div>

                                <?php  }    ?>
                              
                            
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>