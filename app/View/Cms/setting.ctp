<div id="wrapper">
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo BASE_URL ?>/cms/Dashboard">Welcome <?php echo $name; ?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo BASE_URL ?>/cms/profile"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="<?php echo BASE_URL ?>/cms/Setting"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo BASE_URL ?>/Cms/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="<?php echo BASE_URL ?>/cms/Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo BASE_URL ?>/cms/adminsuspendview"><i class="fa fa-dashboard fa-fw"></i>Suspended Admins</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Users<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo BASE_URL ?>/cms/users"><i class="fa fa-dashboard fa-fw"></i>Users Dashboard</a>
                                    
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Events<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo BASE_URL ?>/cms/events"><i class="fa fa-dashboard fa-fw"></i>Events Dashboard</a>
                                    
                                </li>
                                <li>
                                    <a href="<?php echo BASE_URL ?>/cms/unactive_events"><i class="fa fa-dashboard fa-fw"></i>Deactive Events</a>
                                    
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                      
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
<div id="page-wrapper" style="min-height: 339px;">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header">Settings</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
                          
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Change Password
                            

                            <div class="pull-right"> 
                                <a href="<?php echo BASE_URL ?>/cms/Dashboard" class="btn btn-default btn-xs">Go to Dashboard</a>
                            </div>
                            <div class="text-center"> 
                                <?php echo $this->Session->flash();?>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" method="post" name="admin_add" action="<?php echo BASE_URL ?>/cms/setting">
                                    
                                        
                                        <div class="form-group">
                                            <label>Old Password</label>
                                            <input type="password" class="form-control" name="data[User][oldpass]" placeholder="Enter new password" required="" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input type="password" class="form-control" name="data[User][newpass]" placeholder="Confirm new password" required="" autocomplete="off">
                                        </div>
                                        
                                        
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        
                                    </form>
                                </div>
                              
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>