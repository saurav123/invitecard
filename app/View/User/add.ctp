<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>


<div id="wrapper">
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo BASE_URL ?>/user/Dashboard">Welcome <?php echo $name; ?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo BASE_URL ?>/user/profile"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="<?php echo BASE_URL ?>/user/Setting"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo BASE_URL ?>/user/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="<?php echo BASE_URL ?>/user/Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo BASE_URL ?>/user/deactivated_event_view"><i class="fa fa-dashboard fa-fw"></i>Suspended Events</a>
                                </li>
                            </ul>
                        </li>
                        
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                      
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>



        <div id="page-wrapper" style="min-height: 339px;">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header">Settings</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
                          
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create Event
                            

                            <div class="pull-right"> 
                                <a href="<?php echo BASE_URL ?>/user/Dashboard" class="btn btn-default btn-xs">Go to Dashboard</a>
                            </div>
                            <div class="text-center"> 
                                <?php echo $this->Session->flash();?>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form class="forms" role="form" method="post" name="admin_add" action="<?php echo BASE_URL ?>/user/add" enctype="multipart/form-data">
                                    
                                        
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control" name="cover_title" placeholder="Title of Event" required="" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <label>Upload Image</label>
                                              <input name="userImage" type="file" class="inputFile" />
                                        </div><br><br>
                                            <div id="app_1" class="app">
                                                <div class="form-group">
                                                    <label>Ceremony Title</label>
                                                    <input class="form-control" type="text" name="title[]" value=""/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Date of ceremony:</label>
                                                    <input class="form-control datepicker" type="text" name="date[]" value=""/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Time of ceremony:</label>
                                                    <input class="form-control" type="text" name="time[]" value=""/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Address:</label>
                                                    <input class="form-control" type="text" name="address[]" value=""/>
                                                </div>
                                                    <input class="form-control lon" type="hidden" id="lon_1" name="lon[]" value=""/>
                                                    <input class="form-control lat" type="hidden" id="lat_1" name="lat[]" value=""/>
                                                <div style="height: 300px;border: 1px solid #000;" id="map_1" class="map"></div>
                                            </div><br><br>
                                            
                                               
                                        
                                        
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                      
                                    </form><a class=" btn btn-default add" href="javascript:void(0)">ADD MORE CREMONIES</a>
                                </div>
                              
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
                                        <div class="main" style="display: none;">
                                            <div id="app_2" class="app">
                                                <div class="form-group">
                                                    <label>Ceremony Title</label>
                                                    <input class="form-control" type="text" name="title[]" value=""/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Date of ceremony:</label>
                                                    <input class="form-control datepicker type="text" name="date[]" value=""/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Time of ceremony:</label>
                                                    <input class="form-control" type="text" name="time[]" value=""/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Address:</label>
                                                    <input class="form-control" type="text" name="address[]" value=""/>
                                                </div>
                                                    <input class="form-control lon" type="hidden" id="lon_2" name="lon[]" value=""/>
                                                    <input class="form-control lat" type="hidden" id="lat_2" name="lat[]" value=""/>
                                                <div style="height: 300px;border: 1px solid #000;" id="map_2" class="map"></div>
                                                <br>

                                                    <a href="#" class="btn btn-default remove">REMOVE THIS CARD</a>
                                            </div><br><br>
                                        </div>

 

        <script type="text/javascript">
                    $(document).ready(function() {
                            $(".add").click(function(){ 
                                $(".forms").append($('.main').html());

                                var s = document.createElement("script");
                        s.type = "text/javascript";
                        s.text  = window.onload = function() {
                    var latlng = new google.maps.LatLng(30.78,76.69);
                    var map = new google.maps.Map(document.getElementById("map"), {
                        center: latlng,
                        zoom: 11,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: 'Set lat/lon values for this property',
                        draggable: true
                    });
                    google.maps.event.addListener(marker, 'dragend', function(a) {
                        console.log(a);
                        document.getElementById('lat').value=a.latLng.lat().toFixed(4);
                        document.getElementById('lon').value=a.latLng.lng().toFixed(4);


                    });
                };
                        $("#page-wrapper").append(s);
                              



                        var lastdivid = $('.forms').find('.app').last().attr('id');

                        var arr = lastdivid.split('_');

                        var counterdiv = arr[1];

                        counterdiv++;

                        $('.main').find('.app').attr('id','app'+"_"+counterdiv+'');







                        var lastfieldsid = $('.forms').find('.lon').last().attr('id');

                        var arr = lastfieldsid.split('_');

                        var counter = arr[1];

                        counter++;

                        console.log(lastfieldsid);

                        $('.main').find('.lon').last().attr('id','lon'+"_"+counter+'');

                        $('.main').find('.lat').last().attr('id','lat'+"_"+counter+'');

                        $('.main').find('.map').last().attr('id','map'+"_"+counter+'');
                                 
                    


                    });
            
                            $(".forms").on("click",".remove", function(){
                                $(this).parent('div').remove();
                            
                                });
                    });
        </script>

            <script type="text/javascript">
                  window.onload = function() {
                    var latlng = new google.maps.LatLng(30.78,76.69);
                    var map = new google.maps.Map(document.getElementById("map"), {
                        center: latlng,
                        zoom: 11,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: 'Set lat/lon values for this property',
                        draggable: true
                    });
                    google.maps.event.addListener(marker, 'dragend', function(a) {
                        console.log(a);
                        document.getElementById('lat').value=a.latLng.lat().toFixed(4);
                        document.getElementById('lon').value=a.latLng.lng().toFixed(4);


                    });
                };
            </script>

             <script>
                $( function() {
                    $( ".datepicker" ).datepicker();
                    } );
                </script>

