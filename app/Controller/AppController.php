<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array('Session', 'Cookie');
	public $uses = array('Affiliate');

	public function beforeFilter() {

		// Prevent session hijacking
		if ($this -> Session -> check('user') == true) {

			if ($_SERVER['REMOTE_ADDR'] != $this -> Session -> read('user.ip')) {

				$this -> Session -> delete('user');
				$this -> Session -> write('notify.type', 'yellow');
				$this -> Session -> write('notify.title', 'Notification!');
				$this -> Session -> write('notify.message', 'Your session has expired, please login to continue.');
				$this -> redirect(array(
					'controller' => 'clients',
					'action' => 'login'
				));
				die ;

			}
		}
		
		// Track affiliate cookie
		if (isset($_GET['uid']) && ($_GET['uid'] != '')) {

			$affiliate = $this -> Affiliate -> find('first', array(
				'conditions' => array('Affiliate.login_id' => trim($_GET['uid'])),
				'fields' => array('Affiliate.id'),
				'recursive' => 1
			));

			if (!empty($affiliate)) {
				$this -> Cookie -> write('sales.track', $affiliate['Affiliate']['id'], $encrypt = true, $expires = null);
			}

		}

	}

	// Disable caching
	public function beforeRender() {
		$this -> response -> disableCache();

		// Handle errors
		if ($this -> name == 'CakeError') {

			$this -> layout = 'hunter';

		}
	}

	function _captcha() {

		$a = rand(1, MAX_RAND_NB);
		$b = rand(1, MAX_RAND_NB);

		if ($a > $b) {
			$out = "$a - $b";
			$this -> Session -> write('captcha.string', $a . ' - ' . $b);
			$this -> Session -> write('captcha.result', $a - $b);
		} else {
			$out = "$a + $b";
			$this -> Session -> write('captcha.string', $a . ' + ' . $b);
			$this -> Session -> write('captcha.result', $a + $b);
		}
		return $out;
	}

	// Secure form token
	function _token() {

		$token = md5(uniqid(mt_rand(), true));
		$this -> Session -> write('token', $token);
		return $token;

	}

	// Random number generator
	function _randomNum($level = null, $length = null) {
		$chars[1] = "1234567890";
		$chars[2] = "abcdefghijklmnopqrstuvwxyz";
		$chars[3] = "023456789abcdefghijkmnopqrstuvwxyz";
		$chars[4] = "23456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ";
		$chars[5] = "23456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ!@#$%^&*?=+-_";

		$i = 0;
		$str = "";
		while ($i < $length) {
			$str .= $chars[$level][mt_rand(0, strlen($chars[$level]) - 1)];
			$i++;
		}
		return $str;
	}

	// Send email
	function _sendMail($mailTo = array(), $mailSubject = null, $mailTemplate = null, $mailVars = array(), $bcc = array()) {

		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail();
		$email -> from(array('no-reply@oginger.com' => 'oGinger.com'));
		$email -> to($mailTo);
		$email -> bcc($bcc);
		$email -> subject($mailSubject);
		$email -> template($mailTemplate);
		$email -> emailFormat('html');
		$email -> viewVars($mailVars);
		return $email -> send();

	}

	// Send contact email
	function _contactMail($mailFrom = array(), $mailSubject = null, $mailTemplate = null, $mailVars = array()) {

		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail();
		$email -> from($mailFrom);
		$email -> to(array('helpdesk@oginger.com' => 'oGinger.com Helpdesk'));
		$email -> subject($mailSubject);
		$email -> template($mailTemplate, '_default');
		$email -> emailFormat('html');
		$email -> viewVars($mailVars);
		return $email -> send();

	}

}
