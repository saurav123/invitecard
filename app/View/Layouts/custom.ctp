<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo BASE_URL ?>/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo BASE_URL ?>/admin/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo BASE_URL ?>/admin/css/sb-admin-2.css" rel="stylesheet">

    <link href="<?php echo BASE_URL ?>/admin/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo BASE_URL ?>/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

      <!-- DataTables CSS -->
    <link href="<?php echo BASE_URL ?>/admin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo BASE_URL ?>/admin/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>


<?php echo $this->fetch('content'); ?>

    <!-- jQuery -->
    <script src="<?php echo BASE_URL ?>/admin/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo BASE_URL ?>/admin/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo BASE_URL ?>/admin/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo BASE_URL ?>/admin/js/sb-admin-2.js"></script>

     <!-- Morris Charts JavaScript -->
    <script src="<?php echo BASE_URL ?>/admin/raphael/raphael.min.js"></script>
    <script src="<?php echo BASE_URL ?>/admin/morrisjs/morris.min.js"></script>
    <script src="<?php echo BASE_URL ?>/admin/js/morris-data.js"></script>

     <!-- Page-Level Demo Scripts - Notifications - Use for reference -->
  
    <script src="<?php echo BASE_URL ?>/admin/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo BASE_URL ?>/admin/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo BASE_URL ?>/admin/js/sb-admin-2.js"></script>


    <script>
    // tooltip demo
    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })
    // popover demo
    $("[data-toggle=popover]")
        .popover()
    </script>

</body>

</html>
