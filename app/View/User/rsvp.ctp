<div id="wrapper">
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo BASE_URL ?>/user/Dashboard">Welcome <?php echo $name; ?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo BASE_URL ?>/user/profile"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="<?php echo BASE_URL ?>/user/Setting"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo BASE_URL ?>/user/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="<?php echo BASE_URL ?>/user/Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo BASE_URL ?>/user/deactivated_event_view"><i class="fa fa-dashboard fa-fw"></i>Suspended Events</a>
                                </li>
                            </ul>
                        </li>
                       
                        
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                      
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>


         <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Your All Events
                             <div class="pull-right"> 
                                <a href="<?php echo BASE_URL ?>/user/add" class="btn btn-default btn-xs">Add New Event</a>
                            </div>
                        </div>
                       
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php  $msg = $this->Session->flash(); if ($msg) { ?>
                            
                        
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                            <?php echo $msg; ?>
                                    </div>
                                </div>

                              <?php } ?>

                            <div style="margin-bottom: 10px;" class="row">
                                <div class="col-sm-12">
                                    <div class="col-md-2 col-sm-6 pull-left">
                                    <?php 
                                        $limit = (isset($this->params->query['limit']) ? $this->params->query['limit'] : '');
                                        $options = array( 5 => '5 records', 10 => '10 records', 50 => '50 records', 100 => '100 records' );
                                        echo $this->Form->create(array('type'=>'get'));
                                        
                                        
                                        echo $this->Form->select('limit', $options, array(
                                            'value'=>$limit, // For selection
                                            'default'=> 5, 
                                            'empty' => FALSE, 
                                            'onChange'=>'this.form.submit();', 
                                            'name'=>'limit',
                                            'class'=>'form-control input-sm'
                                            )
                                        );
                                        ?>

                                    </div>
                                    
                                </div>
                               
                        </div>


                                    
                        <div class="table-responsive">
             
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id', 'Id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('full_name', 'Full_Name'); ?></th>
                                        <th><?php echo $this->Paginator->sort('mobile', 'Mobile'); ?></th>
                                        <th><?php echo $this->Paginator->sort('response', 'Response'); ?></th>
                                        <th><?php echo $this->Paginator->sort('date_added', 'Date Added'); ?></th>
                                        <th><?php echo $this->Paginator->sort('date_updated', 'Date Updated'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (empty($rsvp)) { ?>
                                            <tr>
                                                <td colspan="7" class="text-center">No Records Found :(</td>
                                            </tr>
                                        <?php } ?>
                                <?php foreach($rsvp as $record) { ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $record['Guest']['id'] ?></td>
                                        <td><?php echo $record['Guest']['full_name'] ?></td>
                                        <td><?php echo $record['Guest']['mobile'] ?></td>
                                        <td><?php echo $record['Guest']['response'] ?></td>
                                        <td class="center"><?php echo $record['Guest']['date_added'] ?></td>
                                        <td class="center"><?php echo $record['Guest']['date_updated'] ?></td>
                                       
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                
                            </table>
                        </div>
                            <!-- /.table-responsive -->
                            <div class="row">
                                <div class="col-lg-6 col-md-4 col-sm-12">
                                    <div class="table_count">
                                        
                                        <?php
                                            echo $this->Paginator->counter(
                                                'Showing {:start} to {:end} of {:count} records'
                                            );
                                        ?> 
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-8 col-sm-12">
                                    <div class="paging_simple_numbers">
                                        <ul class="pagination">
                                            <?php
                                                echo $this->Paginator->first(__('First'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                                                echo $this->Paginator->prev('«', array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                                                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                                                echo $this->Paginator->next('»', array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                                                echo $this->Paginator->last(__('Last'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                                            ?>
                                        </ul>
                                        
                                    </div>
                                </div>
                            </div>
                            
                    </div>
                        <!-- /.panel-body -->
                  
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>