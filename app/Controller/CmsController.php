<?php
class CmsController extends AppController {

	public $uses = array('User','Admin','Event','Ceremony');

	public function beforeFilter()
    {

    	$name = $this->Session->read('Admin.name');

		$this->set(compact('name'));

    	$this->layout = 'custom';
		parent::beforeFilter();

       	if (!$this->Session->check('Admin.id')) {

       	 	//pr($this); die;

       	 	if($this->request->params['action'] != 'index'){

       	 					$this->redirect(array('action'=>'index',

        		 						'controller'=>'cms'));
       	 	}
       	 	
       	}

       	if ($this->Cookie->read('email') !== null) {

       		if($this->request->params['action'] == 'index'){

       	 					$this->redirect(array('action'=>'dashboard',

        		 						'controller'=>'cms'));
       	 	}

       	} 
       	  
	}


	public function index()
    {

    		if ($this->request->is('post')) {

        		$data = $this->request->data;

        		//pr($data); die;
        		$email = $data['email'];
        		$pass =  $data['password'];

        		$record = $this->Admin->find('first', array( 'conditions' => array( 'Admin.email' => $email ,
        		 																	'Admin.password' => $pass )));

        		//pr($record); die;

        		if (!empty($record)) {

        		 	$this->Session->write('Admin.id',$record['Admin']['id']);

        		 	$this->Session->write('Admin.email',$record['Admin']['email']);

        		 	$this->Session->write('Admin.name',$record['Admin']['name']);


        		 	//remember me cookie
        		 	if ($this->request->data['remember'] == 1) {

                		$this->Cookie->write('email', $email = $data['email'], true, '2 weeks');



                		$this->Cookie->write('id', $id = $data['id'], true, '2 weeks');

                        $this->Cookie->write('name', $id = $data['name'], true, '2 weeks');
            		}

        		 	

        		 	$this->redirect(array('action'=>'dashboard',

        		 						'controller'=>'cms'));

        		 }else{

        		 $this->Session->setFlash(__('Username or password is incorrect.'));

        		 }


           
        } 

    	
		
	 }

	  public function logout()
    {

    	$this->Cookie->delete('email');
    	$this->Cookie->delete('id');

    	$this->Session->delete('User.id');

      	$this->redirect(array('action'=>'index',

                        'controller'=>'cms'));
		
	 }

	 public function dashboard()
    {
    	$conditions = array(
            'Admin.id NOT' => $this -> Session -> read('Admin.id'),
            'Admin.status' => 'active'
        	);

			if ($this -> request -> is('get')) {

            	if (isset($this -> params -> query['search']) && !empty($this -> params -> query['search'])) {
					
					$search = $this -> params -> query['search'];

				$searchWords = explode(' ', $search);

                
                    $arr = array();
                    foreach ($searchWords as $searchWord) {
                        $arr[] = array('Admin.id LIKE' => '%' . $searchWord . '%');
                        $arr[] = array('Admin.name LIKE' => '%' . $searchWord . '%');
                        $arr[] = array('Admin.email LIKE' => '%' . $searchWord . '%');
                    }
                    $conditions = array_merge($conditions, array('OR' => $arr));

            }

            
	        }
	         $this -> paginate = array(
	            'conditions' => $conditions,
	            'fields' => array(
		                'Admin.id',
		                'Admin.status',
		                'Admin.name',
		                'Admin.email',
		                'Admin.password',
		                'Admin.date_added',
		                'Admin.date_updated'
		            ),
	            'order' => 'Admin.date_added desc',
	            'paramType' => 'querystring',
	            'limit' => 10,
	            'maxLimit' => 100
	        );
	        $Users = $this -> paginate('Admin');


	        $this -> set(compact('Users'));

         
	 }




	  public function delete($id)
    {
        $this->Admin->delete($id);
        $this->Session->setFlash(__('Record Deleted successfully!'));
        $this->redirect($this->referer());

    }

     public function edit($id)
    { 

        if (!empty($this->request->data)) {
            $this->Admin->save($this->request->data);
            $this->Session->setFlash(__('Record Edited successfully!'));
            $this->redirect(array('action'=>'dashboard',

        		 						'controller'=>'cms'));
        }

        $edits = $this->Admin->find('first', array('conditions' => array('Admin.id' => $id)));
        $this->set(compact('edits'));

    }

	 public function profile()
    {
		$profile = $this->Admin->find('first', array( 'conditions' => array( 'Admin.email' => $this->Session->read('Admin.email'))));

		//pr($profile); die;

		$this->set(compact('profile'));
	 }

	  public function setting()
    {
		if (!empty($this->request->data)) {

			//pr($this->request->data);

			$newpass = $this->request->data['User']['newpass'];

			$confirmpass = $this->request->data['User']['oldpass'];

			$profile = $this->Admin->find('first', array( 'conditions' => array( 'Admin.email' => $this->Session->read('Admin.email'))));

			$oldpass = $profile['Admin']['password'];


			if ($oldpass == $confirmpass) {

			$this -> Admin -> id = $this->Session->read('Admin.id');
            $this -> Admin -> saveField('password', $this->request->data['User']['newpass']);

       		$this->redirect(array('action'=>'profile',

        		 						'controller'=>'cms'));
			}else{
				$this->Session->setFlash(__('password is incorrect.'));
			}


            
        }
	 }

	   public function adminsuspend($id = null)
    {	

    		$profile = $this->Admin->find('first', array( 'conditions' => array( 'Admin.id' => $id)));
			
			$status = 'inactive';

    		$this -> Admin -> id = $id;

            $this -> Admin -> saveField('status', $status);

            $this->Session->setFlash(__('Record Suspended successfully!'));

             $this -> redirect($this -> referer());
        	die ;

       }

       public function adminsuspendview()
    {	
    	//$conditions = $this->Admin->find('all', array( 'conditions' => array( 'Admin.status' => 'inactive' )));

    	$conditions = array(
            'Admin.status' => 'inactive'
        	);


    	if ($this -> request -> is('get')) {

            	if (isset($this -> params -> query['search']) && !empty($this -> params -> query['search'])) {
					
					$search = $this -> params -> query['search'];

				$searchWords = explode(' ', $search);



                
                    $arr = array();
                    foreach ($searchWords as $searchWord) {
                        $arr[] = array('Admin.id LIKE' => '%' . $searchWord . '%');
                        $arr[] = array('Admin.name LIKE' => '%' . $searchWord . '%');
                        $arr[] = array('Admin.email LIKE' => '%' . $searchWord . '%');
                    }
                    $conditions = array_merge($conditions, array('OR' => $arr));

                    //pr($conditions); die;

            }

            
	        }
	         $this -> paginate = array(
	            'conditions' => $conditions,
	            'fields' => array(
		                'Admin.id',
		                'Admin.status',
		                'Admin.name',
		                'Admin.email',
		                'Admin.password',
		                'Admin.date_added',
		                'Admin.date_updated'
		            ),
	            'order' => 'Admin.date_added desc',
	            'paramType' => 'querystring',
	            'limit' => 10,
	            'maxLimit' => 100
	        );
	        $Users = $this -> paginate('Admin');


	        $this -> set(compact('Users'));

    		



       }


       public function adminunsuspend($id = null)
    {	

    		$profile = $this->Admin->find('first', array( 'conditions' => array( 'Admin.id' => $id)));
			
			$status = 'active';

    		$this -> Admin -> id = $id;

            $this -> Admin -> saveField('status', $status);

            $this->Session->setFlash(__('Record UnSuspended successfully!'));

            $this -> redirect($this -> referer());
        	die ;
        

       }

       public function users()
    {
    	$conditions = array(
        	);


    	if ($this -> request -> is('get')) {

            	if (isset($this -> params -> query['search']) && !empty($this -> params -> query['search'])) {
					
					$search = $this -> params -> query['search'];

				$searchWords = explode(' ', $search);



                
                    $arr = array();
                    foreach ($searchWords as $searchWord) {
                        $arr[] = array('User.id LIKE' => '%' . $searchWord . '%');
                        $arr[] = array('User.name LIKE' => '%' . $searchWord . '%');
                        $arr[] = array('User.email LIKE' => '%' . $searchWord . '%');
                    }
                    $conditions = array_merge($conditions, array('OR' => $arr));

                    //pr($conditions); die;

            }

            
	        }

			$this -> paginate = array(
				'conditions' => $conditions,
	            'fields' => array(
	                'User.id',
	                'User.name',
	                'User.email',
	                'User.password',
	                'User.date_added',
	                'User.date_updated'
	            ),
	            'order' => 'User.date_added desc',
	            'paramType' => 'querystring',
	            'limit' => 5,
	            'maxLimit' => 100
	        );
	        $Users = $this -> paginate('User');

	        $this -> set(compact('Users'));

	    }


	    public function events()
    {
    	//$conditions = $this->Event->find('all',array('recursive' => 2));

			$this -> paginate = array(
				'conditions' => array('Event.status' => 'on'),
	            'fields' => array(
	                'Event.id',
	                'User.name',
	                'Event.title',
	                'Event.date_added',
	                'Event.date_updated'
	            ),
	            'order' => 'Event.date_added desc',
	            'paramType' => 'querystring',
	            'limit' => 5,
	            'maxLimit' => 100,
	            'recursive' => 2
	        );
	        $Users = $this -> paginate('Event');

	        

	        $this -> set(compact('Users'));

	        //pr($Users); die;

	    }



	


	     public function event_delete($id)
    {
        $this->Event->delete($id);
        $this->Session->setFlash(__('Record Deleted successfully!'));
        $this->redirect($this->referer());

    }

     public function event_edit($id)
    { 

        if (!empty($this->request->data)) {
            $this->Event->save($this->request->data);
            $this->Session->setFlash(__('Record Edited successfully!'));
            $this->redirect(array('action'=>'events',

        		 						'controller'=>'cms'));
        }

        $edits = $this->Event->find('first', array('conditions' => array('Event.id' => $id)));
        $this->set(compact('edits'));

    }

    public function event_view($id)
    { 

        $edits = $this->Event->find('first', array('conditions' => array('Event.id' => $id)));
        $this->set(compact('edits'));

        $Ceremony = $this->Ceremony->find('all', array('conditions' => array('Ceremony.event_id' => $id)));
        $this->set(compact('Ceremony'));
        //pr($Ceremony); die;

    }

          public function user_delete($id)
    {
        $this->User->delete($id);
        $this->Session->setFlash(__('Record Deleted successfully!'));
        $this->redirect($this->referer());

    }

     public function user_edit($id)
    { 

        if (!empty($this->request->data)) {
            $this->User->save($this->request->data);
            $this->Session->setFlash(__('Record Edited successfully!'));
            $this->redirect(array('action'=>'users',

        		 						'controller'=>'cms'));
        }

        $edits = $this->User->find('first', array('conditions' => array('User.id' => $id)));
        $this->set(compact('edits'));

    }

      public function deactive_event($id = null)
    {	

    		$profile = $this->Event->find('first', array( 'conditions' => array( 'Event.id' => $id)));
			
			$status = 'off';

    		$this -> Event -> id = $id;

            $this -> Event -> saveField('status', $status);

            $this->Session->setFlash(__('Record deactive successfully!'));

             $this -> redirect($this -> referer());
        	die ;

       }


       public function unactive_events()
    {	

    		//$conditions = $this->Event->find('all', array( 'conditions' => array( 'Event.status' => 'off' )));

    		$this -> paginate = array(
    			'conditions' => array('Event.status' => 'off'),
    			
	            'fields' => array(
	                'Event.id',
	                'User.name',
	                'Event.title',
	                'Event.date_added',
	                'Event.date_updated'
	            ),
	            'order' => 'Event.date_added desc',
	            'paramType' => 'querystring',
	            'limit' => 5,
	            'maxLimit' => 100,
	            'recursive' => 2
	            
	        );
	        $Users = $this -> paginate('Event');



          	$this->set(compact('Users'));

       }

       public function activate_event($id = null)
    {	

    		$profile = $this->Event->find('first', array( 'conditions' => array( 'Event.id' => $id)));
			
			$status = 'on';

    		$this -> Event -> id = $id;

            $this -> Event -> saveField('status', $status);

            $this->Session->setFlash(__('Record active successfully!'));

             $this -> redirect($this -> referer());
        	die ;
        

       }


}