<?php

class UserController extends AppController {

	public $uses = array('User','Admin','Event','Ceremony','Wish','Guest');

	public function beforeFilter()
    {	
    	$name = $this->Session->read('User.name');

		$this->set(compact('name'));

    	$name = $this->Session->read('User.name');

		$this->set(compact('name'));

    	$this->layout = 'userdashboard';
		parent::beforeFilter();

       	if (!$this->Session->check('User.id')) {

       	 	//pr($this); die;

       	 	if($this->request->params['action'] != 'index'){

       	 					$this->redirect(array('action'=>'index',

        		 						'controller'=>'cms'));
       	 	}
       	 	
       	}

       	  
	}
	public function index()
    {


	if ($this->request->is('post')) {

        		$data = $this->request->data;

        		//pr($data); die;
        		$email = $data['email'];
        		$pass =  $data['password'];

        		$record = $this->User->find('first', array( 'conditions' => array( 'User.email' => $email ,
        		 																	'User.password' => $pass )));

        		//pr($record); die;

        		if (!empty($record)) {

        		 	$this->Session->write('User.id',$record['User']['id']);

        		 	$this->Session->write('User.email',$record['User']['email']);

        		 	$this->Session->write('User.name',$record['User']['name']);
        		 	

        		 	$this->redirect(array('action'=>'dashboard',

        		 						'controller'=>'User'));

        		 }else{

        		 $this->Session->setFlash(__('Username or password is incorrect.'));

        		 }


           
        } 
    }


     public function logout()
    {

    	$this->Cookie->delete('email');
    	$this->Cookie->delete('id');

    	$this->Session->delete('User.id');

      	$this->redirect(array('action'=>'index',

                        'controller'=>'user'));
		
	 }


	  public function dashboard()
    {
    	$conditions = array(
            'Event.user_id' => $this -> Session -> read('User.id'),
            'Event.status' => 'on'
        	);

			if ($this -> request -> is('get')) {

            	if (isset($this -> params -> query['search']) && !empty($this -> params -> query['search'])) {
					
					$search = $this -> params -> query['search'];

				$searchWords = explode(' ', $search);

                
                    $arr = array();
                    foreach ($searchWords as $searchWord) {
                        $arr[] = array('Event.id LIKE' => '%' . $searchWord . '%');
                        $arr[] = array('Event.title LIKE' => '%' . $searchWord . '%');
                    }
                    $conditions = array_merge($conditions, array('OR' => $arr));

            }

            
	        }
	         $this -> paginate = array(
	            'conditions' => $conditions,
	            'fields' => array(
	            		'Event.id',
		                'Event.title',
		                'Event.date_added',
		                'Event.date_updated'
		            ),
	            'order' => 'Event.date_added desc',
	            'paramType' => 'querystring',
	            'limit' => 10,
	            'maxLimit' => 100
	        );
	        $Users = $this -> paginate('Event');


	        $this -> set(compact('Users'));

         
	 }

	   public function delete_event($id)
    {
        $this->Event->delete($id);
        $this->Session->setFlash(__('Record Deleted successfully!'));
        $this->redirect($this->referer());

    }

     public function profile()
    {
		$profile = $this->User->find('first', array( 'conditions' => array( 'User.email' => $this->Session->read('User.email'))));

		//pr($profile); die;

		$this->set(compact('profile'));
	 }

	 public function setting()
    {
		if (!empty($this->request->data)) {

			//pr($this->request->data);

			$newpass = $this->request->data['User']['newpass'];

			$confirmpass = $this->request->data['User']['oldpass'];

			$profile = $this->User->find('first', array( 'conditions' => array( 'User.email' => $this->Session->read('User.email'))));

			$oldpass = $profile['User']['password'];


			if ($oldpass == $confirmpass) {

			$this -> User -> id = $this->Session->read('User.id');
            $this -> User -> saveField('password', $this->request->data['User']['newpass']);

       		$this->redirect(array('action'=>'profile',

        		 						'controller'=>'user'));
			}else{
				$this->Session->setFlash(__('password is incorrect.'));
			}


            
        }
	 }

	 public function deactivate_event($id = null)
    {	

    		$profile = $this->Event->find('first', array( 'conditions' => array( 'Event.id' => $id)));
			
			$status = 'off';

    		$this -> Event -> id = $id;

            $this -> Event -> saveField('status', $status);

            $this->Session->setFlash(__('Record Deactivated successfully!'));

             $this -> redirect($this -> referer());
        	die ;

       }

        public function deactivated_event_view()
    {	
    	//$conditions = $this->Admin->find('all', array( 'conditions' => array( 'Admin.status' => 'inactive' )));

    	$conditions = array(
            'Event.status' => 'off'
        	);


    	if ($this -> request -> is('get')) {

            	if (isset($this -> params -> query['search']) && !empty($this -> params -> query['search'])) {
					
					$search = $this -> params -> query['search'];

				$searchWords = explode(' ', $search);



                
                    $arr = array();
                    foreach ($searchWords as $searchWord) {
                        $arr[] = array('Event.id LIKE' => '%' . $searchWord . '%');
                        $arr[] = array('Event.title LIKE' => '%' . $searchWord . '%');
                    }
                    $conditions = array_merge($conditions, array('OR' => $arr));

                    //pr($conditions); die;

            }

            
	        }
	         $this -> paginate = array(
	            'conditions' => $conditions,
	            'fields' => array(
		              'Event.id',
		                'Event.title',
		                'Event.date_added',
		                'Event.date_updated'
		            ),
	            'order' => 'Event.date_added desc',
	            'paramType' => 'querystring',
	            'limit' => 10,
	            'maxLimit' => 100
	        );
	        $Users = $this -> paginate('Event');


	        $this -> set(compact('Users'));

    		



       }

       public function activate_event($id = null)
    {	

    		$profile = $this->Event->find('first', array( 'conditions' => array( 'Event.id' => $id)));
			
			$status = 'on';

    		$this -> Event -> id = $id;

            $this -> Event -> saveField('status', $status);

            $this->Session->setFlash(__('Record Activated successfully!'));

            $this -> redirect($this -> referer());
        	die ;
        

       }



       public function add($id = null)
    {	

    	App::import('Vendor','geo/geoplugin');


			$geoplugin = new geoPlugin();

			$geoplugin->locate();
			 
			//echo "Geolocation results for {$geoplugin->ip}: <br />\n".
			  
			  //"Longitude: {$geoplugin->longitude} <br />\n".
			  //"Latitude: {$geoplugin->latitude} <br />\n";

    	 if (!empty($_POST)) {

                  $data = $this->request->data;
                  //pr($data); die;

                      $final = explode(".", $_FILES['userImage']['name']);

                      $final[0] .= mt_rand(100000,999999);

                      $new_name = implode(".",$final);

                      //pr($data['title']); die;

            if (is_uploaded_file($_FILES['userImage']['tmp_name'])) {

                      $sourcePath = $_FILES['userImage']['tmp_name'];

                      $targetPath = WWW_ROOT . 'uploads' . DS  . $new_name;

                      move_uploaded_file ($sourcePath , $targetPath );

                      
              } 
             

              $edit['user_id'] = $this->Session->read('User.id');

              $edit['secret_id'] = 'sdhfnvlsdiohfos';

              $edit['img'] = $new_name;

              $edit['title'] = $data['cover_title'];

              $edit['date_added'] = date('Y-m-d H:i:s');

              //$this->Event->create();

              $this->Event->save($edit);

              $edits = $this->Event->find('first', array('conditions' => array('Event.title' => $data['cover_title'])));
              //pr($edits); die;
        	  $this->set(compact('edits'));

              unset($data['cover_title']);

              unset($data['userImage']);

              


              $first_array = reset($data);

                      $count_array = count($first_array);
                      $a = array();
                     
                      foreach ($data as $k => $v) {
                        foreach ($v as $key => $value) {
                          $a[$key]['Ceremony'][$k] = $value;

                        }

                     }
                     
             
                     foreach ($a as $save_data) {
                            $save_data['Ceremony']['event_id'] = $edits['Event']['id'];
                            $save_data['Ceremony']['user_id'] = $this->Session->read('User.id');
                            $save_data['Ceremony']['secret_id'] = $edit['secret_id'];
                            $save_data['Ceremony']['date_added'] = date('Y-m-d H:i:s');
                            $this->Ceremony->create();
                            $this->Ceremony->save($save_data);
                     }



              $this->redirect(array('action'=>'dashboard',

                        'controller'=>'user'));
      }


       }


       public function event_view($id)
    { 

        $edits = $this->Event->find('first', array('conditions' => array('Event.id' => $id)));
        $this->set(compact('edits'));

        $Ceremony = $this->Ceremony->find('all', array('conditions' => array('Ceremony.event_id' => $id)));
        $this->set(compact('Ceremony'));


    }


     public function wishes($id)
    { 

    	$this -> paginate = array(
    			'conditions' => array('Wish.event_id' => $id),
	            'order' => 'Wish.date_added desc',
	            'paramType' => 'querystring',
	            'limit' => 5,
	            'maxLimit' => 100,
	            'recursive' => 2
	            
	        );
	        $Wishes = $this -> paginate('Wish');


        $this->set(compact('Wishes'));


    }


     public function rsvp($id)
    { 


        $this -> paginate = array(
    			'conditions' => array('Guest.event_id' => $id),
	            'order' => 'Guest.date_added desc',
	            'paramType' => 'querystring',
	            'limit' => 5,
	            'maxLimit' => 100,
	            'recursive' => 2
	            
	        );
	        $rsvp = $this -> paginate('Guest');

	        
        $this->set(compact('rsvp'));


    }

}